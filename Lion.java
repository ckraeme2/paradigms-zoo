// Defines the lion class

import java.util.*;

class Lion extends Mammal{
  boolean hasMane;
  String eatingHabits;

  Lion(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.hasMane=false;
    this.eatingHabits="eats a lot"; 
  } 
	
  void setMane(){
    boolean correctAns = false;
    while(!correctAns){
    	System.out.println("Does the lion have a mane? (Yes/No)");
    	Scanner in = new Scanner(System.in);
    	String ans = in.next();
    	if(ans.equals("Yes")){
     		 this.hasMane = true;
		 correctAns = true;
    	}
    	else if(ans.equals("No")){
      		this.hasMane = false;
		 correctAns = true;

    	}
    	else{
      	System.out.println("Error: provide a yes or no answer");
    	}
  	}
  }
  
  void setEatingHabits(){
    System.out.println("What's its eating habit?");
    Scanner in_2 = new Scanner(System.in);
    eatingHabits = in_2.nextLine();
  }
	

	// Prints a description of a lion, including the values of its unique properties  
  void print(){
	  if(this.hasMane){
	  	System.out.println(this.name + " is a " + this.species + " who has a mane and " + this.eatingHabits);
	  }
	  else{
		  System.out.println(this.name + " is a " +  this.species + " who does not have a mane and  " +  this.eatingHabits);
	  }
  }
	

  String getHasMane(){
	  if(this.hasMane){
		  return "Has a mane";
	  }
	  else{
		  return "Does not have a mane";
	  }
  }

  String getEatingHabits() {
	  return this.eatingHabits;
  }

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}

  String getConservationStatus() {
	  return this.conservationStatus;
   }
}
