// Defines the Reptile class


class Reptile extends Animal{
  boolean livesUnderwater;
  boolean shedsSkin;
  Reptile(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1, s2, s3);
    this.livesUnderwater = b1;
	this.shedsSkin = b2;
  }

  String getLivesUnderwater() {
	  if(this.livesUnderwater){
		  return "Lives underwater";
	  }
	  else{
		  return "Does not live underwater";
	  }
  }

  String getShedsSkin() {
	  if(this.shedsSkin){
		  return "Sheds skin";
	  }
	  else{
		  return "Does not shed skin";
      }
   }
}

