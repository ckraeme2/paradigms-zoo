// Defines the Eagle class


import java.util.*;

class Eagle extends Bird{
  boolean attacksPeople;
  boolean isBald;

  Eagle(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.attacksPeople = true;
    this.isBald = false;
  } 

  void setAttacksPeople(){
    System.out.println("Does the eagle attack people? (Yes/No)");
    Scanner in = new Scanner(System.in);
    String ans = in.next();
    if(ans.equals("Yes")){
      this.attacksPeople = true;
    }
    else if(ans.equals("No")){
      this.attacksPeople = false;
    }
    else{
      System.out.println("Error: provide a yes or no answer");
    }
  }

   void setIsBald(){
     System.out.println("Is the eagle bald? (Yes/No)");
     Scanner in = new Scanner(System.in);
     String ans = in.next();
     if(ans.equals("Yes")){
       this.isBald = true;
     }
     else if(ans.equals("No")){
       this.isBald = false;
     }
     else{
       System.out.println("Error: provide a yes or no answer");
     }
   }

	// Prints a description of an Eagle object, including the values of all of its unique properties
  void print(){
	  System.out.print(this.name + " is a " + this.species + " who will ");
	  if(this.attacksPeople){
	  	System.out.print("attack people and ");
	  }
	  else{
	  	System.out.print("not attack people and ");
	  }
	  if(this.isBald){
	  	System.out.println("is bald");
	  }
	  else{
	  	System.out.println("is not bald");
	  }
  }

	String getAttacksPeople(){
		if(this.attacksPeople){
			return "Attacks people";
		}
		else{
			return "Does not attack people";
	    }
     }

	String getIsBald() {
		if(this.isBald){
			return "Is bald";
		}
		else{
			return "Is not bald";
		}
	}

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}

  String getConservationStatus() {
	  return this.conservationStatus;
	}
}
