// Defines the Fish class


class Fish extends Animal{
  boolean hasScales;
  boolean hasGills;
  Fish(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1, s2, s3);
    this.hasScales = b1;
	this.hasGills = b2;
  }

  String getHasScales() {
	  if(this.hasScales){
		  return "Has scales";
	  }
	  else{
		  return "Does not have scales";
	  }
  }

  String getHasGills() {
	  if(this.hasGills){
		  return "Has gills";
	  }
	  else{
		  return "Does not have gills";
	  }
  }
}

