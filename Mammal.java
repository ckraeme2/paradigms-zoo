// Defines the Mammal class

class Mammal extends Animal{
  boolean hasClaws;
  boolean hasFur;
  Mammal(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1, s2, s3);
    this.hasClaws = b1;
	this.hasFur = b2;
  }

  String getHasClaws() {
	  if(this.hasClaws){
		  return "Has claws";
	  }
	  else{
		  return "Does not have claws";
	  }
  }

  String getHasFur() {
	  if(this.hasFur){
		  return "Has fur";
	  }
	  else{
		  return "Does not have fur";
	  }
   }
}

