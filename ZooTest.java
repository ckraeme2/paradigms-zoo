// Defines the ZooTest class


import java.util.*;

class ZooTest {
 	static int numAnimals = 9;
	static List<Animal> myAnimals=new ArrayList<Animal>(); 
	static int numLion = 1;
	static int numDolphin = 1;
	static int numEagle = 1;
	static int numOstrich = 1;
	static int numLizard = 1;
	static int numCrocodile = 1;
	static int numShark = 1;
	static int numGoldfish = 2; 
  
  public static void main(String[] args) {
    boolean simulate = true;
    setUpAnimals();
    while(simulate){		// Repeatedly prompts user to choose an option
      System.out.println("\nType exit at any time to end the program.\nOptions:\nadd - add an animal\ndelete- delete an animal\ndisplay- display an animal\nverbose- display verbose list of animals\nsummary: show summary of all animals \n\nSelect option: ");
      Scanner in = new Scanner(System.in); 
      String ans = in.next();
      if (ans.equals("add")){
        addAnimal();    // calls add animal function
      }
  	  else if (ans.equals("delete")){
        deleteAnimal();      // calls delete animal function
      }
    else if (ans.equals("display")){
        displayAnimals();     	// calls display animal function
      }
      else if (ans.equals("verbose")){
      	printVerboseList();        // calls verbose list function
      }
      else if (ans.equals("summary")){
      	printSummary();         // calls summary function
      }
      else if(ans.equals("exit")){
        simulate = false;      // exit the options menu; end the program
      }
      else{
        System.out.println("Error. Please enter a number corresponding to one of the options or 'exit' to quit the program.");
      }
      

    }
  }

  public static void setUpAnimals(){
  	
   // Initializes a zoo full of animals of each species	  

	myAnimals.add(new Lion("Bob","Lion","not extinct",true,true));
	myAnimals.add(new Dolphin("Alex","Dolphin","endangered",false,false));
	myAnimals.add(new Eagle("Philip", "Eagle", "endangered", false, true));
	myAnimals.add(new Ostrich("Kayla", "Ostrich", "not extinct",false ,false));
	myAnimals.add(new Lizard("Tammy", "Lizard","not extinct", false,true));
	myAnimals.add(new Crocodile("Lindsey", "Crocodile", "not extinct", true, false));
	myAnimals.add(new Shark("Adam", "Shark", "not extinct", true, true));
	myAnimals.add(new Goldfish("Neo", "Goldfish", "not extinct", true, true));
	myAnimals.add(new Goldfish("Katrina","Goldfish", "not extinct", true, true));

	printSummary();
	printVerboseList();

  }

  static void printSummary(){

	// Prints the total size of the zoo and the number of animals of each species

  	System.out.println("Summary: \nThere are "+ myAnimals.size() + " animals in the zoo.");
	System.out.println(numLion +" Lion(s)");
	System.out.println(numDolphin + " Dolphin(s)");
	System.out.println(numEagle + " Eagle(s)");
	System.out.println(numOstrich + " Ostrich(es)");
	System.out.println(numLizard + " Lizard(s)" );
	System.out.println(numCrocodile + " Crocodile(s)");
	System.out.println(numShark + " Shark(s)");
	System.out.println(numGoldfish + " Goldfish");
	System.out.println();
  }
  
  static void printVerboseList(){

	// Prints a list of the animals ordered in terms of when they were added

	int numAnimals = myAnimals.size();
	System.out.println("\nVerbose List of Animals with details:");

	for(int i =0; i<numAnimals; i++){
		String s = myAnimals.get(i).getAnimalSpecies();
		if(s.equals("Lion")){
			System.out.print((i+1) + ": "); 
			Lion l =((Lion)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Dolphin")){
			System.out.print((i+1) + ": "); 
			Dolphin l =((Dolphin)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Eagle")){
			System.out.print((i+1) + ": "); 
			Eagle l =((Eagle)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Ostrich")){
			System.out.print((i+1) + ": "); 
			Ostrich l =((Ostrich)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Lizard")){
			System.out.print((i+1) + ": "); 
			Lizard l =((Lizard)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Crocodile")){
			System.out.print((i+1) + ": "); 
			Crocodile l =((Crocodile)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Shark")){
			System.out.print((i+1) + ": "); 
			Shark l =((Shark)myAnimals.get(i));
			l.print();
		}
		else if(s.equals("Goldfish")){
			System.out.print((i+1) + ": "); 
			Goldfish l =((Goldfish)myAnimals.get(i));
			l.print();
		}
	
	}

  }  

  public static void addAnimal() {

	  // adds an animal
    System.out.println("What animal would you like to add to the zoo?");
    Scanner in = new Scanner(System.in);
    String species = in.next();
    String name = in.next();
    if( species.equals("Lion")){   // conditionals to determine which species constructor to call
      Lion l = new Lion(name,  species, "Not extinct", true, true);
      l.setMane();
      l.setEatingHabits();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Claws: " + l.getHasClaws() + "\nHas Fur: " + l.getHasFur() + "\nHas Mane: " + l.getHasMane() + "\nEating Habits: " + l.getEatingHabits());
      myAnimals.add(l);
      numLion ++;

    }
    else if( species.equals("Dolphin")){
      Dolphin l = new Dolphin(name,  species, "endangered", false, false);
      l.setFin();
      l.setSing();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Claws: " + l.getHasClaws() + "\nHas Fur: " + l.getHasFur() + "\nHas large fin: " + l.getFin() + "\nLikes to sing: " + l.getLikesToSing());
      myAnimals.add(l);
      numDolphin++;
    }
    else if( species.equals("Eagle")){
      Eagle l = new Eagle(name,  species, "endangered", false, true);
      l.setAttacksPeople();
      l.setIsBald();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Long Beak: " + l.getHasLongBeak() + "\nCan Fly: " + l.getCanFly() + "\nAttacks People: " + l.getAttacksPeople() + "\nIs Bald: " + l.getIsBald());
      myAnimals.add(l);
      numEagle++;
    }
    else if( species.equals("Ostrich")){
      Ostrich l = new Ostrich(name,  species, "not extinct", false, false);
      l.setDrinksWater();
      l.setLivesWith();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Long Beak: " + l.getHasLongBeak() + "\nCan Fly: " + l.getCanFly() + "\nDrinks Water: " + l.getDrinksWater() + "\nLives With: " + l.getLivesWith());
      myAnimals.add(l);
      numOstrich ++;

    }
    else if( species.equals("Lizard")){
      Lizard l = new Lizard(name,  species, "not extinct", false, true);
      l.setTail();
      l.setVenom();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nLives Under Water: " + l.getLivesUnderwater() + "\nShed skin: " + l.getShedsSkin() + "\nSheds Tail: " + l.getShedsTail() + "\nIs Venomous: " + l.getIsVenomous());
      myAnimals.add(l);
      numLizard++;
    }
    else if( species.equals("Crocodile")){
      Crocodile l = new Crocodile(name,  species, "not extinct", true, false);
      l.setTeeth();
      l.setHostile();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nLives Under Water: " + l.getLivesUnderwater() + "\nShed skin: " + l.getShedsSkin() + "\nHas Sharp Teeth: " + l.getHasSharpTeeth() + "\nIs Hostile: " + l.getIsHostile());
      myAnimals.add(l);
      numCrocodile++;
    }
    else if( species.equals("Shark")){
      Shark l = new Shark(name,  species, "not extinct", true, true);
      l.setTeeth();
      l.setClimate();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Scales: " + l.getHasScales() + "\nHas Gills: " + l.getHasGills() + "\nHas Sharp Teeth: " + l.getHasSharpTeeth() + "\nClimate: " + l.getClimate());
      myAnimals.add(l);
      numShark++;
    }
    else if( species.equals("Goldfish")){
      Goldfish l = new Goldfish(name,  species, "not extinct", true, true);
      l.setBright();
      l.setFood();
	  System.out.println("Name: "+ l.getName() + "\nSpecies: " + l.getSpecies() + "\nConservation Status: " + l.getConservationStatus() + "\nHas Scales: " + l.getHasScales() + "\nHas Gills: " + l.getHasGills() + "\nHas Sharp Teeth: " + l.getIsBright() + "\nfavoriteFood: " + l.getFavoriteFood());
      myAnimals.add(l);
      numGoldfish++;
    }
    else{
      System.out.println("Error: species not recognized");
    }
  }

  public static void deleteAnimal() {

	// deletes an animal

	  System.out.println("What is the name of the animal you would like to remove?");
	  Scanner in = new Scanner(System.in);
	  String delName = in.next();
	  boolean validName = false;  // checks if a name is found in the list
	  for(int i =0; i < myAnimals.size(); i++){
		if(delName.equals(myAnimals.get(i).name)){
			validName = true;   // animal of given name was found
		
			if(myAnimals.get(i).species.equals("Lion")){
				numLion--;  // decrement the total variable when an animal of a given spcecies is deleted
			}
			else if(myAnimals.get(i).species.equals("Dolphin")){
				numDolphin--;
			}
			else if(myAnimals.get(i).species.equals("Eagle")){
				numEagle--;
			}
			else if(myAnimals.get(i).species.equals("Ostrich")){
				numOstrich--;
			}
			else if(myAnimals.get(i).species.equals("Lizard")){
				numLizard--;
			}
			else if(myAnimals.get(i).species.equals("Crocodile")){
				numCrocodile--;
			}
			else if(myAnimals.get(i).species.equals("Shark")){
				numShark--;
			}
			else if(myAnimals.get(i).species.equals("Goldfish")){
				numGoldfish--;
			}
			myAnimals.remove(i);
			System.out.println(delName + " has been removed from the zoo");
			break;
		}		
	}
	if (validName==false){
		System.out.println(delName+" not found in the zoo.");
	}
	
  }

  public static void displayAnimals() {

	// display information on an animal of a given name

	System.out.println("What is the name of the animal you would like to display information on?");
	Scanner in = new Scanner(System.in);
	String dispName = in.next();
	boolean validName = false;  // checks if an animal corresponding to the name exists in the zoo

	for(int i = 0; i < myAnimals.size(); i++){
		if(dispName.equals(myAnimals.get(i).name)){
			if(myAnimals.get(i).species.equals("Lion")){  // type cast the animal variable depending on its species property
				validName = true;
				((Lion)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Dolphin")){
				validName = true;
				((Dolphin)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Eagle")){
				validName = true;
				((Eagle)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Ostrich")){
				validName = true;
				((Ostrich)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Shark")){
				validName = true;
				((Shark)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Goldfish")){
				validName = true;
				((Goldfish)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Lizard")){
				validName = true;
				((Lizard)myAnimals.get(i)).print();
				break;
			}
			else if(myAnimals.get(i).species.equals("Crocodile")){
				validName = true;
				((Crocodile)myAnimals.get(i)).print();
				break;
			}


		}

  	}
	if (validName==false){
		System.out.println(dispName+" not found in the zoo.");
	}

  }




}

