// Defines the Ostrich class


import java.util.*;

class Ostrich extends Bird{
  boolean drinksWater;
  String livesWith;

  Ostrich(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.drinksWater=false;
    this.livesWith="a large group";
  } 

  void setDrinksWater(){
    boolean correctAns=false;
    while (! correctAns){

    System.out.println("Does the ostrich drink water? (Yes/No)");
    Scanner in = new Scanner(System.in);
    String ans = in.next();
    if(ans.equals("Yes")){
      this.drinksWater = true;
      correctAns = true;
    }
    else if(ans.equals("No")){
      this.drinksWater = false;
      correctAns = true;
    }
    else{
      System.out.println("Error: provide a yes or no answer");
    }
    }
  }
  
  void setLivesWith(){
    System.out.println("Who does the ostrich live with?");
    Scanner in_2 = new Scanner(System.in);
    livesWith = in_2.nextLine();
  }


	// Prints a description of an Ostrich object, including the values of all of its unique properties  
  void print(){
	  if(this.drinksWater){
	  	System.out.println(this.name + " is a " + this.species + " who loves drinking water and lives with" + this.livesWith);
	  }
	  else{
		  System.out.println(this.name + " is a " +  this.species + " who hates drinking water and lives with" +  this.livesWith);
	  }
  }

  String getDrinksWater() {
	  if(this.drinksWater){
		  return "Drinks water";
	  }
	  else{
		  return "Does not drink water";
	  }
  }

	String getLivesWith() {
		return livesWith;
	}

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}

  String getConservationStatus() {
	  return this.conservationStatus;
	}
}
