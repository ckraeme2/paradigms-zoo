// Defines the Lizard class


import java.util.*;

class Lizard extends Reptile{
  boolean shedsTail;
  boolean isVenomous;

  Lizard(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.shedsTail = true;
    this.isVenomous = false;
  } 

  void setTail(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Does the lizard shed its tail? (Yes/No)");
    	Scanner in = new Scanner(System.in);
    	String ans = in.next();
    	if(ans.equals("Yes")){
			correctAns = true;
      	this.shedsTail = true;
    	}
    	else if(ans.equals("No")){
			correctAns = true;
      	this.shedsTail = false;
    	}
    	else{
      	System.out.println("Error: provide a yes or no answer");
    	}
	}
  }
  
  void setVenom(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Is the lizard venomous? (Yes/No)");
    	Scanner in_2 = new Scanner(System.in);
    	String ans = in_2.next();
		if(ans.equals("Yes")){
			correctAns = true;
			this.isVenomous = true;
		}
		else if(ans.equals("No")){
			correctAns = true;
			this.isVenomous = false;
		}
		else{
			System.out.println("Error: provide a yes or no answer");
		}	
  	}
}


	// Prints a description of a Lizard object, including the values of all of its unique properties
  void print(){
	  if(this.shedsTail && this.isVenomous){
		  System.out.println(this.name + " is a " + this.species + " who sheds its tail and is venomous ");
	  }
	  else if(this.shedsTail){
		  System.out.println(this.name + " is a " + this.species + " who sheds its tail and is not venomous");
	  }
	  else if(this.isVenomous){
		  System.out.println(this.name + " is a " + this.species + " who does not shed its tail and is venomous");
	  }
	  else{
		  System.out.println(this.name + " is a " + this.species + " who does not shed its tail and is not venomous");
	  }
  }
	
  String getShedsTail() {
	  if(this.shedsTail){
		  return "Sheds tail";
	  }
	  else{
		  return "Does not shed tail";
	  }
   }

	String getIsVenomous() {
		if(this.isVenomous){
			return "Is venomous";
		}
		else{
			return "Is not venomous";
		}
	}

  String getName() {
	  return this.name;
  }

  String getSpecies() {
	  return this.species;
  }

  String getConservationStatus() {
	  return this.conservationStatus;
  }
}

  
