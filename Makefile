all: simple

Animal.class: /usr/bin/java ZooTest.java
	@javac -cp . Animal.java Bird.java Fish.java Mammal.java Reptile.java Lion.java Dolphin.java Eagle.java Ostrich.java Shark.java Goldfish.java Lizard.java Crocodile.java ZooTest.java

simple: Animal.class 
	@echo Running instance of the ZooTest system...
	@java ZooTest

clean:
	rm -rf *.class

