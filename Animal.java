// Defines the Animal class

class Animal{
  String name;
  String species;
  String conservationStatus;

  Animal(String s1, String s2, String s3){
    this.name = s1;
    this.species = s2;
    this.conservationStatus = s3;
  }

  public String getAnimalSpecies(){		// Gets the species value of a given animal
  	return this.species;
  }

}

