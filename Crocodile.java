// Defines the Crocodile class 


import java.util.*;

class Crocodile extends Reptile{
  boolean hasSharpTeeth;
  boolean isHostile;

  Crocodile(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.hasSharpTeeth = true;
    this.isHostile = true;
  } 

  void setTeeth(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Does the crocodile have sharp teeth? (Yes/No)");
    	Scanner in = new Scanner(System.in);
    	String ans = in.next();
    	if(ans.equals("Yes")){
			correctAns = true;
      		this.hasSharpTeeth = true;
    	}
    	else if(ans.equals("No")){
			correctAns = true;
      		this.hasSharpTeeth = false;
    	}
    	else{
      		System.out.println("Error: provide a yes or no answer");
    	}
	}
  }
  
  void setHostile(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Is the crocodile hostile towards humans? (Yes/No)");
    	Scanner in_2 = new Scanner(System.in);
    	String ans = in_2.next();
		if(ans.equals("Yes")){
			correctAns = true;
			this.isHostile = true;
		}
		else if(ans.equals("No")){
			correctAns = true;
			this.isHostile = false;
		}
		else{
			System.out.println("Error: provide a yes or no answer");
		}
  	}
	}
	
  
  // Prints an object of the crocodile class, including the values of all of its unique properties
  void print(){
	  if(this.hasSharpTeeth && this.isHostile){
		  System.out.println(this.name + " is a " + this.species + " who has sharp teeth and is hostile to humans");
	  }
	  else if(this.hasSharpTeeth){
		  System.out.println(this.name + " is a " + this.species + " who has sharp teeth but is not hostile to humans");
	  }
	  else if(this.isHostile){
		  System.out.println(this.name + " is a " + this.species + " who does not have sharp teeth but is hostile to humans");
	  }
	  else{
		  System.out.println(this.name + " is a " + this.species + " who does not have sharp teeth and is not hostile to humans");
	  }
  }
	
	String getHasSharpTeeth() {
		if(this.hasSharpTeeth){
			return "Has sharp teeth";
		}
		else{
			return "Does not have sharp teeth";
		}
	}

	String getIsHostile() {
		if(this.isHostile){
			return "Is hostile";
		}
		else{
			return "Is not hostile";
		}
	}

  String getName() {
	  return this.name;
  }

  String getSpecies() {
	  return this.species;
  }

  String getConservationStatus() {
	  return this.conservationStatus;
  }
}


