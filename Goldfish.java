// Defines the Goldfish class


import java.util.*;

class Goldfish extends Fish{
  boolean isBright;
  String favoriteFood;

  Goldfish(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.isBright = true;
    this.favoriteFood = "bread crumbs"; 
  } 

  void setBright(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Is the goldfish colored bright? (Yes/No)");
    	Scanner in = new Scanner(System.in);
    	String ans = in.next();
    	if(ans.equals("Yes")){
			correctAns = true;
      		this.isBright = true;
    	}
    	else if(ans.equals("No")){
			correctAns = true;
      		this.isBright = false;
    	}
    	else{
      		System.out.println("Error: provide a yes or no answer");
    	}
  	}
}
  
  void setFood(){
    System.out.println("What is the goldfish's favorite food?");
    Scanner in_2 = new Scanner(System.in);
    favoriteFood = in_2.nextLine();
  }


	// Prints a description of a Goldfish object, inclduing the values of all of its unique properties
  void print(){
	  if(this.isBright){
	  	System.out.println(this.name + " is a " + this.species + " who is bright and whose favorite food is " + this.favoriteFood);
	  }
	  else{
		  System.out.println(this.name + " is a " +  this.species + " who is not bright and whose favorite food is  " +  this.favoriteFood);
	  }
  }
	
	String getIsBright() {
		if(this.isBright){
			return "Is bright";
	    }
		else{
			return "Is not bright";
		}
	}

	String getFavoriteFood() {
		return this.favoriteFood;
	}

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}

  String getConservationStatus() {
	  return this.conservationStatus;
  }

}
	
