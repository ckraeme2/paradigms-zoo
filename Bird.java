// Defines the Bird class


class Bird extends Animal{
  boolean hasLongBeak;
  boolean canFly;
  Bird(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1, s2, s3);
    this.hasLongBeak = b1;
	this.canFly = b2;
  }

  String getHasLongBeak() {
	  if(this.hasLongBeak){
		  return "Has a long beak";
	  }
	  else{
		  return "Does not have a long beak";
	  }
   }

  String getCanFly() {
	  if(this.canFly){
		  return "Can fly";
	  }
	  else{
		  return "Cannot fly";
	  }
  }
}

