// Defines the dolphin class


import java.util.*;

class Dolphin extends Mammal{
  boolean hasLargeFin;
  boolean likesToSing;

  Dolphin(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.hasLargeFin = true;
    this.likesToSing = true;
  } 

  void setFin(){
    boolean correctAns=false;
    while (! correctAns){
    System.out.println("Does the dolphin have a large fin? (Yes/No)");
    Scanner in = new Scanner(System.in);
    String ans = in.next();
    if(ans.equals("Yes")){
      this.hasLargeFin = true;
      correctAns=true;
    }
    else if(ans.equals("No")){
      this.hasLargeFin = false;
      correctAns=true;
    }
    else{
      System.out.println("Error: provide a yes or no answer");
    }
    }
  }
  
  void setSing(){
     boolean correctAns=false;
    while (! correctAns){

    System.out.println("Does the dolphin like to sing? (Yes/No)");
    Scanner in_2 = new Scanner(System.in);
    String ans = in_2.next();
	if(ans.equals("Yes")){
		this.likesToSing = true;
		correctAns=true;

	}
	else if(ans.equals("No")){
		this.likesToSing = false;
		correctAns=true;
	}
	else{
		System.out.println("Error: provide a yes or no answer");
	}
     }
  }



	// Prints a description of the dolphin, including the values of all of its unique properties  
  void print(){
	  if(this.hasLargeFin && this.likesToSing){
	  	System.out.println(this.name + " is a " + this.species + " who has a large fin and likes to sing");
	  }
	  else if(this.hasLargeFin){
		  System.out.println(this.name + " is a " +  this.species + " who has a large fin and does not like to sing");
	  }
	  else if(this.likesToSing){
		 System.out.println(this.name + " is a " + this.species + " who does not have a large fin and likes to sing");
	  }
	  else{
		 System.out.println(this.name + " is a " + this.species + " who does not have a large fin and does not like to sing");
	  } 
  }

  String getFin() {
	 if(this.hasLargeFin){
		return "Has a large fin";
	 }
	 else{
		return "Does not have a large fin";
     }
   }

  String getLikesToSing(){
	  if(this.likesToSing){
		  return "Likes to sing";
	  }
	  else{
		  return "Does not like to sing";
	  }
  }

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}
  String getConservationStatus(){
  	  return this.conservationStatus;
  }
}

