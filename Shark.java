// Defines an object of the Shark class



import java.util.*;

class Shark extends Fish{
  boolean hasSharpTeeth;
  String climate;

  Shark(String s1, String s2, String s3, boolean b1, boolean b2){
    super(s1,s2,s3,b1,b2);
    this.hasSharpTeeth = true;
    this.climate = "Pacific Ocean";
  } 

  void setTeeth(){
	boolean correctAns = false;
	while(!correctAns){
    	System.out.println("Does the shark have sharp teeth? (Yes/No)");
    	Scanner in = new Scanner(System.in);
    	String ans = in.next();
    	if(ans.equals("Yes")){
			correctAns = true;
      		this.hasSharpTeeth = true;
    	}
    	else if(ans.equals("No")){
			correctAns = true;
      		this.hasSharpTeeth = false;
    	}
    	else{
      	System.out.println("Error: provide a yes or no answer");
    	}
	}
  }
  
  void setClimate(){
    System.out.println("What kind of climate does the shark prefer?");
    Scanner in_2 = new Scanner(System.in);
    climate = in_2.nextLine();
  }
	
  
  // Prints a description of a Shark object, including the values of all of its unique properties
  void print(){
	  if(this.hasSharpTeeth){
	  	System.out.println(this.name + " is a " + this.species + " who has sharp teeth and its preferred climate is " + this.climate);
	  }
	  else{
		  System.out.println(this.name + " is a " +  this.species + " who does not have sharp teeth and  " +  this.climate);
	  }
  }
	

  String getHasSharpTeeth(){
	  if(this.hasSharpTeeth){
		  return "Has sharp teeth";
	  }
	  else{
		  return "Does not have sharp teeth";
	  }
   }

  String getClimate() {
	  return this.climate;
   }

  String getName() {
	return this.name;
	}

  String getSpecies() {
	  return this.species;
	}
	
  String getConservationStatus() {
	  return this.conservationStatus;
  }

}
